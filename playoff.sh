#!/bin/bash

URL="https://www.basketball-reference.com"
read -p "Digite o nome sobrenome do jogador: " jogador

FILE=/tmp/playoffdump
NOMES=$(echo $jogador | sed "s/ /\n/g" | wc -l)

if [ $NOMES -eq 1 ]
then
	echo "Para funcionamento do script, é necessário prover nome e sobrenome"
	exit 10
fi

LETRA=$(echo $jogador | sed "s/^.* //" | cut -c1 | tr A-Z a-z)
PLAYER_LINK=$(curl -s ${URL}/players/$LETRA/ | grep -i "$jogador" | grep -i href | egrep -o "/players.*\.html" | sort -u)
PLAYOFF_LINK=$(curl -s ${URL}${PLAYER_LINK} | grep -i "gamelog" | grep "<li>" | grep -i playoff | sed "s/^.*href=\"//" | cut -d \" -f1 | sort -u)

curl -s "${URL}${PLAYOFF_LINK}" > $FILE

echo "Digite o mínimo necessário para cada atributo. Caso não queira que este atributo seja levado em consideração, deixe o campo vazio."
sleep 1
read -p "Digite o mínimo de pontos necessários: " PTS_MIN
read -p "Digite o mínimo de assistências necessárias: " ASTS_MIN
read -p "Digite o mínimo de rebotes necessários: " REB_MIN
read -p "Digite o mínimo de roubos necessários: " STL_MIN
read -p "Digite o mínimo de tocos necessários: " BLK_MIN
read -p "Digite o mínimo de bolas de 3 necessárias: " THRPT_MIN
read -p "Digite o aproveitamento minimo para as bolas de 3 (ex: 50%): " THRPP_MIN

if [[ ! $THRPP_MIN =~ ^[0-9]{1,3}\%$ ]]
then
	if [ ! -z $THRPP_MIN ]
	then
		echo "Formato inválido"
		exit
	fi
else
	if [ -z $THRPT_MIN ]
	then
		THRPT_MIN=1
	fi
fi
read -p "Digite o aproveitamento minimo para os arremessos de quadra (ex: 50%): " FGP_MIN
if [[ ! $FGP_MIN =~ ^[0-9]{1,3}\%$ ]]
then
	if [ ! -z $FGP_MIN ]
	then
		echo "Formato inválido"
		exit
	fi
fi

valida_game()
{
	VALID=0
	if [ -z $PTS_MIN ]
	then
		echo > /dev/null
	else
		[ $PTS -lt $PTS_MIN ] 2> /dev/null && VALID=1
	fi

	if [ -z $ASTS_MIN ]
	then
		echo > /dev/null
	else
		[ $AST -lt $ASTS_MIN ] 2> /dev/null && VALID=1
	fi

	if [ -z $REB_MIN ]
	then
		echo > /dev/null
	else
		[ $REB -lt $REB_MIN ] 2> /dev/null && VALID=1
	fi

	if [ -z $STL_MIN ]
	then
		echo > /dev/null
	else
		[ $STL -lt $STL_MIN ] 2> /dev/null && VALID=1
	fi

	if [ -z $BLK_MIN ]
	then
		echo > /dev/null
	else
		[ $BLK -lt $BLK_MIN ] 2> /dev/null && VALID=1
	fi

	if [ -z $THRPT_MIN ]
	then
		echo > /dev/null
	else
		[ $THRP -lt $THRPT_MIN ] 2> /dev/null && VALID=1
	fi

	if [ -z $THRPP_MIN ]
	then
		echo > /dev/null
	else
		THRPP_MIN=$(echo $THRPP_MIN | egrep -o "[0-9]{1,3}")
		THRPPC=$(echo $THRPP | egrep -o "[0-9]{1,3}")
		[ $THRPPC -lt $THRPP_MIN ] 2> /dev/null && VALID=1
	fi

	if [ -z $FGP_MIN ]
	then
		echo > /dev/null
	else
		FGP_MIN=$(echo $FGP_MIN | egrep -o "[0-9]{1,3}")
		FGPC=$(echo $FGP | egrep -o "[0-9]{1,3}")
		[ $FGPC -lt $FGP_MIN ] 2> /dev/null && VALID=1
	fi

	[ $VALID -eq 0 ] && echo "DATA: $DATA Vs: $ADV PTS: $PTS | ASTS: $AST | REBS: $REB | STL: $STL | BLK: $BLK | 3PT: $THRP | FG%: ${FGP}% | 3PT%: ${THRPP}%"
}

for i in $(seq $(grep "<tr id=" $FILE | wc -l))
do
	PTS=$(grep "<tr id=" $FILE | head -n$i | tail -n1 | egrep -o 'data-stat=\".{1,8}\"..[0-9]{1,3}' | grep pts | egrep -o "[0-9]{1,3}")
	AST=$(grep "<tr id=" $FILE | head -n$i | tail -n1 | egrep -o 'data-stat=\".{1,8}\"..[0-9]{1,3}' | grep ast | egrep -o "[0-9]{1,3}")
	REB=$(grep "<tr id=" $FILE | head -n$i | tail -n1 | egrep -o 'data-stat=\".{1,8}\"..[0-9]{1,3}' | grep trb  | egrep -o "[0-9]{1,3}")
	STL=$(grep "<tr id=" $FILE | head -n$i | tail -n1 | egrep -o 'data-stat=\".{1,8}\"..[0-9]{1,3}' | grep stl | egrep -o "[0-9]{1,3}")
	BLK=$(grep "<tr id=" $FILE | head -n$i | tail -n1 | egrep -o 'data-stat=\".{1,8}\"..[0-9]{1,3}' | grep blk | egrep -o "[0-9]{1,3}")
	FGA=$(grep "<tr id=" $FILE | head -n$i | tail -n1 | egrep -o 'data-stat=\".{1,8}\"..[0-9]{1,3}' | grep fga | egrep -o "[0-9]{1,3}")
	FG=$(grep "<tr id=" $FILE | head -n$i | tail -n1 | egrep -o 'data-stat=\".{1,8}\"..[0-9]{1,3}' | grep fg\" | egrep -o "[0-9]{1,3}")
	ADV=$(grep "<tr id=" $FILE | head -n$i | tail -n1 | sed "s/<[^>]*//g" | tr -s ">" | sed -E "s/\((\+|\-)[0-9].*$//" | egrep -o "[A-Za-z]{3}" | tail -n1)
	DATA=$(grep "<tr id=" $FILE | head -n$i | tail -n1 | egrep -o "[0-9]{4}\-[0-9]{2}\-[0-9]{2}" | sort -u)
	THRPA=$(grep "<tr id=" $FILE | head -n$i | tail -n1 | egrep -o 'data-stat=\".{1,8}\"..[0-9]{1,3}' | grep "fg3a" | sed "s/^.*>//")
	THRP=$(grep "<tr id=" $FILE | head -n$i | tail -n1 | egrep -o 'data-stat=\".{1,8}\"..[0-9]{1,3}' | grep "fg3\"" | sed "s/^.*>//")
	FGP=$(echo "scale=2; $FG / $FGA" | bc 2> /dev/null)
	THRPP=$(echo "scale=2; $THRP / $THRPA" | bc 2> /dev/null)
	valida_game 
done | nl
